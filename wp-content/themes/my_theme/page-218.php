<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>
<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>
	<div id="primary" <?php astra_primary_class(); ?>>
		<?php
			astra_primary_content_top();

			$paged= get_query_var('pg');

			$args = [
				'post_type'=> 'proyecto',
				'post_per_page'=> 10,
				'paged'=> $paged,
			];

			$query = new WP_Query( $args );
		?>

	
<div class="proyectos-container">
	<h1 class="title-proyectos">Mis Proyectos</h1>
	<p class="text-proyectos">Echa un vistazo a algunos proyectos que desarrollado.</p>
        <ul class="cards-list">
            <?php while ($query->have_posts()) {
                $query->the_post();
                global $post;
            ?>
                <li class="card">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <?php echo get_the_post_thumbnail($post->ID); ?>
                        <div class="card-content">
                            <h2 class="title-card"><?php echo get_the_title(); ?></h2>
                            <?php $imagen = get_field('imagen'); ?>
                            <img class="foto-persona" src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>">
                        </div>
                    </a>
                </li>
            <?php } ?>
        </ul>

        <div class="pagination">
            <ul>
                <?php echo paginate_links([
                    'total' => $query->max_num_pages,
                    'current' => $paged,
                    'format' => '?pg=%#%'
                ]); ?>
            </ul>
        </div>

        <?php wp_reset_postdata(); ?>
    </div>


		<?php
			astra_primary_content_bottom();
		?>
	</div><!-- #primary -->
<?php
if ( astra_page_layout() == 'right-sidebar' ) :

	get_sidebar();

endif;

get_footer();
