<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if (astra_page_layout() == 'left-sidebar'): ?>

    <?php get_sidebar(); ?>

<?php endif ?>

<div id="primary" <?php astra_primary_class(); ?>>

    <?php astra_primary_content_top(); ?>

    <?php //astra_content_loop(); ?>

    <?php
    if (have_posts()):
        while (have_posts()):
            the_post(); ?>

            <div class="project-detail">
                <h2 class="post-title"><?php the_title(); ?></h2>
                <div class="post-content">
                    <?php the_content(); ?>
                    <?php $imagen = get_field('imagen'); ?>
                    <img class="project-image" src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>">

                    <div class="project-info">
                        <p class="project-area"><span class="subtitle">Área:</span> <?php echo get_field('area'); ?></p>
                        <p class="project-description"><span class="subtitle">Descripción:</span>
                            <?php echo get_field('descripcion'); ?></p>
                        <?php $link = get_field('link'); ?>
                        <?php if ($link): ?>
                            <a class="project-link-button" href="<?php echo $link; ?>" target="_blank">Ver Proyecto</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php
        endwhile;
    endif;
    ?>

    <?php astra_primary_content_bottom(); ?>

</div><!-- #primary -->

<?php if (astra_page_layout() == 'right-sidebar'): ?>

    <?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>